# TP GLI 2048

## Etudiants

Cette application est crée par Ouhammouch Salma & Assal Mohamed Rida 
Etudiants en Master2 MITIC

## Repertoire

Ce repertoire contient le jeu 2048 en FWML pour le module GLI

## Lancement

Pour lancer le jeu il faut lancer le fichier Main2048.java qui se trouve dans src.

OU

Lancer le jar "TP_2048-0.0.1-SNAPSHOT.jar"