
import java.io.IOException;

import controller.Controller;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main2048 extends Application {

	@Override
	public void start(Stage primaryStage) throws IOException {
            FXMLLoader fxmlLoader = new FXMLLoader();
            Parent root = fxmlLoader.load(Main2048.class.getResource("view/2048fx.fxml").openStream());
            Controller ctrl = fxmlLoader.getController();            
            primaryStage.setTitle("2048");
            primaryStage.setScene(new Scene(root, 500, 450));
            primaryStage.show();
            ctrl.setStage(primaryStage);
	}

	public static void main(String[] args) {
		launch(args);
	}
}
