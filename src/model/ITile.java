package model;

/**
 * Created by plouzeau on 2014-10-09.
 */
public interface ITile {

    int getRank();

    void setRank(int rank);

    void incrementRank();

}
