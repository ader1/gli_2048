package model;

/**
 * Created by plouzeau on 2014-10-09.
 */
public class Tile implements ITile  {

    private  int rank;

    public Tile(int rank) {
        this.rank = rank;
    }

    @Override
    public int getRank() {
        return this.rank;
    }

    @Override
    public void incrementRank() {
        this.rank++;
    }

    @Override
    public void setRank(int rank) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
