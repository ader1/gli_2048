package controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import model.Board;
import model.IBoard;
import model.Tile;

public class Controller implements Initializable{

    @FXML
    GridPane gridPane;
    
    @FXML
    Label labelScore;
    
    @FXML
    Label labelDeplacement;
    
    private ObservableList<Node> tileLabel;
    private Board board;
    private Stage stage;
    private boolean win, lost;  
    private int nbdeplacement = 0;
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        newGame();
        this.updateGrid();
        this.win = false;
        this.lost = false;
        this.nbdeplacement = 0;
    }
    
    public void newGame() {
        this.board = new Board(4);
        tileLabel=gridPane.getChildren();
        for(int i=0; i<= 15;i++){
            ((Label) (tileLabel.get(i))).setText("");
        }
    }
    
    @FXML
    public void clickTryAgain(){
        newGame();
        this.updateGrid();
        this.win = false;
        this.lost = false;
        this.nbdeplacement = 0;
        this.labelDeplacement.setText(this.nbdeplacement+"");
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
        
        this.tileLabel = gridPane.getChildren();
        this.stage.getScene().setOnKeyPressed(new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent event) {
                switch(event.getCode()){
                    case LEFT:
                        Controller.this.move(IBoard.Direction.LEFT);
                        break;
                    case RIGHT:
                        Controller.this.move(IBoard.Direction.RIGHT);
                        break;
                    case UP:
                        Controller.this.move(IBoard.Direction.TOP);
                        break;
                    case DOWN:
                        Controller.this.move(IBoard.Direction.BOTTOM);
                        break;
                }
            }
        });
    }
    
    public void move(IBoard.Direction dir){
        
        //System.out.println("here  "+h);
        this.board.packIntoDirection(dir);
        this.board.commit();
        this.updateGrid();
        labelDeplacement.setText(nbdeplacement +"");;
        this.win = this.hasWon();
        this.lost = this.hasLost();
        this.nbdeplacement++;
    }

    private void updateGrid() {
        labelScore.setText(this.board.getScore()+"");
        for(int i=0;i<this.board.getSideSizeInSquares();i++){
            for(int j=0;j<this.board.getSideSizeInSquares();j++){
                Tile tile = this.board.getTile(i+1, j+1);
                int rank=0;
                if(tile!=null){
                    rank=tile.getRank();
                }
                if(rank==0){
                    ((Label) (tileLabel.get((i * 4) + j))).setText("");
                }else{
                    String newValue = String.format("%.0f", Math.pow(2, rank));
                    ((Label) (tileLabel.get((i * 4) + j))).setText(newValue);
                }  
            }
        }
        if (this.hasWon()){
            final Stage aler = new Stage();
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            aler.initOwner(this.stage);
            alert.setTitle("Congratulations");
            alert.setContentText("YOU WIN!");
            aler.setAlwaysOnTop(true);
            aler.setIconified(true);
            aler.alwaysOnTopProperty();
            alert.show();
            this.newGame();
            this.updateGrid();
            this.win = false;
            this.lost = false;
            this.nbdeplacement = 0;
            this.labelDeplacement.setText(this.nbdeplacement+"");
        }
        if(this.hasLost()){
            final Stage aler = new Stage();
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            aler.initOwner(this.stage);
            alert.setTitle("Sorry");
            alert.setContentText("YOU LOST!");
            aler.setAlwaysOnTop(true);
            aler.setIconified(true);
            aler.alwaysOnTopProperty();
            alert.show();
            this.newGame();
            this.updateGrid();
            this.win = false;
            this.lost = false;
            this.nbdeplacement = 0;
            this.labelDeplacement.setText(this.nbdeplacement+"");
        }
    }
    public boolean hasWon() {
        for (int i = 1; i <= 4; i++) {
            for (int j = 1; j <= 4; j++) {
                if (board.getTile(i, j) != null && board.getTile(i, j).getRank() == 11) {
                    return true;
                }
            }
        }
        return false;
    }
	
    
    public boolean hasLost() {
    return !(ableToMove(IBoard.Direction.BOTTOM)
                    ||ableToMove(IBoard.Direction.TOP)
                    ||ableToMove(IBoard.Direction.LEFT)
                    ||ableToMove(IBoard.Direction.RIGHT));
    }

    private boolean ableToMove(IBoard.Direction direction) {
        int offsetX = 0, offsetY = 0;
        int lowerX = 1, lowerY = 1;
        int upperX = 4, upperY = 4;
        
        switch (direction) {
            case LEFT :
                offsetX = - 1;
                lowerX = 2;
                break;
            case RIGHT:
                offsetX = + 1;
                upperX = 4 - 1;
                break;
            case BOTTOM:
                offsetY = + 1;
                upperY = 4 - 1;
                break;
            case TOP:
                offsetY = - 1;
                lowerY = 2;
        }
        
        for (int i = lowerY; i <= upperY; i++) {
            for (int j = lowerX; j <= upperX; j++) {
                if (board.getTile(i, j) != null) {
                    if (board.getTile(i + offsetY, j + offsetX) == null || board.getTile(i, j).getRank() == board.getTile(i + offsetY, j + offsetX).getRank()) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
}
